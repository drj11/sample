# sample

Output a certain fraction of input lines

## SYNOPSIS

```
sample [-f fraction] [file ...]
```

### DESCRIPTION

Copy approximately 1% of the input lines to the output.
The rest are discarded.

With no _file_ arguments, or when _file_ is «-»,
standard input is used.

Each line is selected independently and pseudo-randomly by
hashing the line number and the line, and thresholding the
resulting hash values.
This implies:

`sample` is approximate.
In the sense that the number of lines sampled is
only approximately the right fraction.

`sample` is deterministic.
For the same input it will give the same output.

`sample` is monotonic.
In the sense that increasing _fraction_ only ever adds lines to
the sample: the set of lines that are samples at fraction
_p_ are a subset of the lines that are sampled for larger values
of _p_.

**`-f fraction`**

The fraction of input lines that should be sampled.
This fraction should be given as a decimal with a decimal point.
The default is 0.01 (1%).
