package main

import (
	"bufio"
	"encoding/binary"
	"flag"
	"hash/fnv"
	"io"
	"log"
	"os"
)

var fp = flag.Float64("f", 0.01, "fraction of input that is sampled")

func main() {
	flag.Parse()

	arg := flag.Args()
	if len(arg) == 0 {
		arg = []string{"-"}
	}

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	for _, p := range arg {
		r, err := Open(p)
		if err != nil {
			log.Println(err)
			continue
		}

		inp := bufio.NewReader(r)

		err = Sample(inp, *fp, out)
		if err != nil {
			log.Println(err)
		}
	}

}

func Sample(inp *bufio.Reader, f float64, out io.Writer) (err error) {
	// Convert f to an integer between 0 and 2**64
	f *= 18446744073709551616.0 // 2 ** 64 exact
	n := uint64(f)
	i := uint64(0)

	for {
		line, err := inp.ReadBytes('\n')
		if len(line) > 0 {
			h := fnv.New64a()
			binary.Write(h, binary.LittleEndian, i)
			h.Write(line)
			if h.Sum64() < n {
				out.Write(line)
			}
		}
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		i += 1
	}
}

func Open(path string) (io.Reader, error) {
	if path == "-" {
		return os.Stdin, nil
	}
	return os.Open(path)
}
